from flask import Flask, request
from twilio.twiml.voice_response import VoiceResponse, Gather
from selenium import webdriver
from threading import Thread
import time

GECKO_DRIVER_PATH = '/home/landon/Downloads/geckodriver'

app = Flask(__name__)
rubber_chicken_price = None

@app.route('/')
def hello_world():
    return ':('


@app.route("/voice", methods=['GET', 'POST'])
def voice():
    print("hello")

    """Respond to incoming phone calls with a menu of options"""
    # Start our TwiML response
    resp = VoiceResponse()

    # Start our <Gather> verb
    gather = Gather(num_digits=1, action='/gather')
    gather.say('Hey there friend! Please press 1 for ay lmao, 2 for nothing, and 3 for the rubber chicken index.')
    resp.append(gather)

    # If the user doesn't select an option, redirect them into a loop
    resp.redirect('/voice')

    print(str(resp))

    return str(resp)


@app.route('/gather', methods=['GET', 'POST'])
def gather():
    global rubber_chicken_price
    """Processes results from the <Gather> prompt in /voice"""
    # Start our TwiML response
    resp = VoiceResponse()

    # If Twilio's request to our app included already gathered digits,
    # process them
    if 'Digits' in request.values:
        # Get which digit the caller chose
        choice = request.values['Digits']

        # <Say> a different message depending on the caller's choice
        if choice == '3':
            rubber_chicken_price_tokens = rubber_chicken_price[1:].split(".")
            resp.say('The rubber chicken index is at ' + str(rubber_chicken_price_tokens[0]) + ' dollars and ' + str(rubber_chicken_price_tokens[1]) + ' cents!')
            return str(resp)
        elif choice == '1':
            resp.say('Ay lmao')
            return str(resp)
        else:
            # If the caller didn't choose 1 or 2, apologize and ask them again
            resp.say("Fucking hell, enter a good number.")

    # If the user didn't choose 1 or 2 (or anything), send them back to /voice
    resp.redirect('/voice')

    return str(resp)


def get_price(url):
    driver = webdriver.Firefox(executable_path=GECKO_DRIVER_PATH)

    driver.get(url)
    time.sleep(3)
    assert 'Amazon.com' in driver.title

    price = driver.find_element_by_css_selector('#priceblock_ourprice').text

    time.sleep(2)
    driver.close()

    return price


def watch_rubber_chicken_index():
    global rubber_chicken_price
    while True:
        rubber_chicken_price = get_price("https://www.amazon.com/Screaming-Shrilling-Christmas-Anti-anxiety-Depression/dp/B07CZ9R3C9/ref=pd_lpo_vtph_21_tr_t_2?_encoding=UTF8&psc=1&refRID=N8CVR3K1EKN0X3DM45GQ")
        print(rubber_chicken_price)
        time.sleep(1800)

if __name__ == "__main__":
    watch_price_thread = Thread(target = watch_rubber_chicken_index)
    watch_price_thread.start()
    app.run(debug=False, host="0.0.0.0", port="6969")

